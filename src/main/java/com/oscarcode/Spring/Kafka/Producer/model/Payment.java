package com.oscarcode.Spring.Kafka.Producer.model;

public class Payment {

    private int total;
    private String currency;
    private String description;
    private String invoice_number;

    public Payment(int total, String currency, String description, String invoice_number) {
        this.total = total;
        this.currency = currency;
        this.description = description;
        this.invoice_number = invoice_number;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInvoice_number() {
        return invoice_number;
    }

    public void setInvoice_number(String invoice_number) {
        this.invoice_number = invoice_number;
    }


}
