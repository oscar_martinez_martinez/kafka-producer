package com.oscarcode.Spring.Kafka.Producer.resource;

import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.oscarcode.Spring.Kafka.Producer.model.Dog;
import com.oscarcode.Spring.Kafka.Producer.model.Payment;
import com.oscarcode.Spring.Kafka.Producer.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("kafka")
public class UserResource {

    @JsonRawValue
    String json;


    @Autowired
    private KafkaTemplate<String, Payment> kafkaTemplatePayment;

    private static final String TOPIC_PAYMENT = "Payment";

    @PostMapping("/sendPayment")
    public String postPayment(@RequestBody final Payment payment) throws JsonProcessingException {

        ObjectWriter ow = new ObjectMapper().writer();
        json = ow.writeValueAsString(payment);

        //for (int i = 0; i < 100000 ; i++) {
        kafkaTemplatePayment.send(TOPIC_PAYMENT, payment);
        //}

        return "Message Published Successfully";
    }

}
